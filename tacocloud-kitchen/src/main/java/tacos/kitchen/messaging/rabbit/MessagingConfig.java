package tacos.kitchen.messaging.rabbit;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class MessagingConfig {
	
	@Bean
	public MessageConverter messageConverter() {
		
		Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();
		log.info("messageConverter: " + messageConverter);
		return messageConverter;
	}
}
