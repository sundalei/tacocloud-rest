package tacos.kitchen.messaging.rabbit;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import tacos.Order;
import tacos.kitchen.OrderReceiver;

@Component
@Slf4j
public class RabbitOrderReceiver implements OrderReceiver {
	private RabbitTemplate rabbit;
	
	private MessageConverter converter;

	@Autowired
	public RabbitOrderReceiver(RabbitTemplate rabbit) {
		this.rabbit = rabbit;
		this.converter = rabbit.getMessageConverter();
	}

	@Override
	public Order receiveOrder() {
		return rabbit.receiveAndConvert("tacocloud.order", new ParameterizedTypeReference<Order>() {
		});
	}

	/*
	@Override
	public Order receiveOrder() {
		Message message = rabbit.receive("tacocloud.order");
		if(message != null) {
			log.info("message: " + message);
			return (Order)converter.fromMessage(message);
		} else {
			log.info("message: null");
			return null;
		}
	}
	*/
}
