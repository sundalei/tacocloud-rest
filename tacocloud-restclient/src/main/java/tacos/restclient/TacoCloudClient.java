package tacos.restclient;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import tacos.Ingredient;
import tacos.Taco;

@Service
@Slf4j
public class TacoCloudClient {
	
	private RestTemplate rest;
	private Traverson traverson;
	
	public TacoCloudClient(RestTemplate rest, Traverson traverson) {
		this.rest = rest;
		this.traverson = traverson;
	}
	
	/*
	public Ingredient getIngredientById(String ingredientId) {
		Map<String, String> urlVariables = new HashMap<>();
		urlVariables.put("id", ingredientId);
		URI url = UriComponentsBuilder
				.fromHttpUrl("http://localhost:8080/ingredientsx/{id}")
				.build(urlVariables);
		
		return rest.getForObject(url, Ingredient.class);
	}
	*/
	
	public Ingredient getIngredientById(String ingredientId) {
		Map<String, String> urlVariables = new HashMap<>();
		urlVariables.put("id", ingredientId);
		URI url = UriComponentsBuilder
				.fromHttpUrl("http://localhost:8080/ingredientsx/{id}")
				.build(urlVariables);
		
		ResponseEntity<Ingredient> responseEntity = rest.getForEntity(url, Ingredient.class);
		log.info("Fetched time: " + responseEntity.getHeaders().getDate());
		
		return responseEntity.getBody();
	}
	
	public Ingredient createIngredient(Ingredient ingredient) {
		HttpEntity<String> entity = this.setEntity(ingredient);
		ResponseEntity<Ingredient> responseEntity = 
				rest.postForEntity("http://localhost:8080/ingredientsx", entity, Ingredient.class);
		
		log.info("New Resource created at "+ responseEntity.getHeaders().getLocation());
		
		return responseEntity.getBody();
	}
	
	public void updateIngredient(Ingredient ingredient) {
		HttpEntity<String> entity = this.setEntity(ingredient);
		rest.put("http://localhost:8080/ingredientsx/{id}", entity, ingredient.getId());
	}
	
	public void deleteIngredient(Ingredient ingredient) {
		rest.delete("http://localhost:8080/ingredientsx/{id}", ingredient.getId());
	}
	
	public Iterable<Ingredient> getAllIngredientsWithTraverson() {
		ParameterizedTypeReference<Resources<Ingredient>> ingredientType =
				new ParameterizedTypeReference<Resources<Ingredient>>() {};
				
		Resources<Ingredient> ingredientRes = 
				traverson.follow("ingredients").toObject(ingredientType);
		
		Collection<Ingredient> ingredients = ingredientRes.getContent();
		return ingredients;
	}

	public Iterable<Taco> getAllTacosWithTraverson() {
		ParameterizedTypeReference<Resources<Taco>> tacoType =
				new ParameterizedTypeReference<Resources<Taco>>() {};

		Resources<Taco> tacoResources =
				traverson.follow("tacos", "recents").toObject(tacoType);

		Collection<Taco> tacos = tacoResources.getContent();
		return tacos;
	}

	public Ingredient addIngredient(Ingredient ingredient) {
		String ingredientUrl = traverson.follow("ingredients").asLink().getHref();
		log.info("ingredientUrl: " + ingredientUrl);
		HttpEntity<String> entity = this.setEntity(ingredient);
		ResponseEntity<Ingredient> responseEntity =
				rest.postForEntity(ingredientUrl, entity, Ingredient.class);

		log.info("New Resource created at "+ responseEntity.getHeaders().getLocation());

		return responseEntity.getBody();
	}
	
	private HttpEntity<String> setEntity(Ingredient ingredient) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ObjectMapper mapper = new ObjectMapper();
		String ingredientJson = null;
		try {
			ingredientJson = mapper.writeValueAsString(ingredient);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("json: " + ingredientJson);
		
		HttpEntity<String> entity = new HttpEntity<String>(ingredientJson, headers);
		return entity;
	}
}
