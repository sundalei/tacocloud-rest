package tacos.restclient;

import java.net.URI;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import tacos.Ingredient;
import tacos.Taco;

@SpringBootConfiguration
@ComponentScan
@Slf4j
public class RestExample {
	
	public static void main(String[] args) {
		SpringApplication.run(RestExample.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public Traverson traverson() {
		Traverson traverson = new Traverson(URI.create("http://localhost:8080/api"), MediaTypes.HAL_JSON);
		return traverson;
	}
	
//	@Bean
//	public CommandLineRunner fetchIngredients(TacoCloudClient tacoCloudClient) {
//		return args -> {
//			Ingredient ingredient = tacoCloudClient.getIngredientById("CHED");
//			log.info("Ingredient: " + ingredient);
//		};
//	}
	
//	@Bean
//	public CommandLineRunner createIngredient(TacoCloudClient tacoCloudClient) {
//		return args -> {
//			Ingredient ingredient = new Ingredient("TEST", "test", Ingredient.Type.WRAP);
//			Ingredient result = tacoCloudClient.createIngredient(ingredient);
//			log.info("Ingredient created: " + result);
//		};
//	}
	
//	@Bean
//	public CommandLineRunner updateIngredient(TacoCloudClient tacoCloudClient) {
//		return args -> {
//			Ingredient ingredient = new Ingredient("TEST", "test update", Ingredient.Type.WRAP);
//			tacoCloudClient.updateIngredient(ingredient);
//			log.info("success");
//		};
//	}
	
//	@Bean
//	public CommandLineRunner deleteIngredient(TacoCloudClient tacoCloudClient) {
//		return args -> {
//			Ingredient ingredient = new Ingredient("TEST", "test update", Ingredient.Type.WRAP);
//			tacoCloudClient.deleteIngredient(ingredient);
//			log.info("success");
//		};
//	}
	
//	@Bean
//	public CommandLineRunner traversonGetIngredients(TacoCloudClient tacoCloudClient) {
//		return args -> {
//			Iterable<Ingredient> allIngredients = tacoCloudClient.getAllIngredientsWithTraverson();
//			for(Ingredient ingredient : allIngredients) {
//				log.info("  -  " + ingredient);
//			}
//		};
//	}

//	@Bean
//	public CommandLineRunner traversonGetTacos(TacoCloudClient tacoCloudClient) {
//		return args -> {
//			Iterable<Taco> allTacos = tacoCloudClient.getAllTacosWithTraverson();
//			for(Taco taco : allTacos) {
//				log.info("  -  " + taco);
//			}
//		};
//	}

//	@Bean
//	public CommandLineRunner addIngredient(TacoCloudClient tacoCloudClient) {
//		return args -> {
//			Ingredient ingredient = new Ingredient("TESU", "test update", Ingredient.Type.WRAP);
//			tacoCloudClient.addIngredient(ingredient);
//			log.info("success");
//		};
//	}
}
