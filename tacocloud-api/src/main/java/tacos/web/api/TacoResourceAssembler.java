package tacos.web.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import tacos.Taco;

public class TacoResourceAssembler extends ResourceAssemblerSupport<Taco, TacoResource> {
	
	private static final Logger log = LoggerFactory.getLogger(TacoResourceAssembler.class);

	public TacoResourceAssembler() {
		super(DesignTacoController.class, TacoResource.class);
	}

	@Override
	protected TacoResource instantiateResource(Taco taco) {
		log.info("TacoResourceAssembler instantiateResource method invoked.");
		System.out.println("TacoResourceAssembler instantiateResource method invoked.");
		return new TacoResource(taco);
	}

	@Override
	public TacoResource toResource(Taco taco) {
		log.info("TacoResourceAssembler toResource method invoked.");
		System.out.println("TacoResourceAssembler toResource method invoked.");
		return createResourceWithId(taco.getId(), taco);
	}

}
